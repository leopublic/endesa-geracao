<?
include("inc/topo.php");
include("inc/class/FusionCharts.php");
?>
    <div style="width:48%; float: left;">
        <?
        $mes = mysql_fetch_object($bd->executa("SELECT acesInterno interno, acesExterno externo, acesMes FROM acessos"))or die(mysql_error());
        
        $strXML = "<graph caption='Acessos no mês atual' numberPrefix='' formatNumberScale='0' decimalPrecision='0'>";
    	$strXML .= "<set name='Acessos internos' value='".$mes->interno."' color='".getFCColor()."' />";
    	$strXML .= "<set name='Acessos externos' value='".$mes->externo."' color='".getFCColor()."' />";
    	$strXML .= "</graph>";
        
        print renderChart("inc/Charts/FCF_Column3D.swf", "", $strXML, "Acessos", 300, 300);
        ?>
    </div>
    <div style="width:48%; float: right;">
        <?
        $strXML = "<graph caption='Acessos Mensais' numberPrefix='' formatNumberScale='0' decimalPrecision='0'>";
        
        $_ano = $bd->executa("SELECT * FROM historicoacesso ORDER BY histAno DESC, histMes ASC");
        while($ano=mysql_fetch_object($_ano)){
        	$strXML .= "<set name='".$ano->histMes."/".$ano->histAno."' value='".$ano->histAcesso."' color='".getFCColor()."' />";
        }
    	$strXML .= "</graph>";
        
        print renderChart("inc/Charts/FCF_Column3D.swf", "", $strXML, "AcessosAnuais", 400, 300);
        ?>
    </div>
<?
include("inc/footer.php");
?>
