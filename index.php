<?
session_start();
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
header("Cache-Control: no-store, no-cache, must-revalidate"); 
header("Cache-Control: post-check=0, pre-check=0", false); 
header("Pragma: no-cache");
header("Content-Type:text/html; charset=iso-8859-1");

$pagina = '' ;
if (isset($_SESSION['pagina'])){
	$pagina = $_SESSION['pagina'];
}
unset($_SESSION['pagina']);
if ($pagina == '' ){
	$pagina = 'inicio.php';
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" >
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" >
		<meta name="Author" content="M2 Software (http://www.m2software.com.br" /> 
		<meta name="KeyWords" content="Energia, Eletricidade, Rio de Janeiro, Cachoeira Dourada, Fortaleza, Usinas, Endesa" /> 
		<meta name="Description" content="Empresa privada no setor de energia."/> 
		<meta name="country" content="Brazil" /> 
		<meta name="organization-Email" content="jssantos@endesabr.com.br" /> 
		<meta name="copyright" content="copyright 2007/2009 - Endesa Gera��o Brasil" /> 
		<meta name="coverage" content="Worldwide" /> 
		<meta name="revisit_after" content="15days" /> 
		<meta name="title" content="Endesa Gera��o Brasil" /> 
		<meta name="identifier" content="http://www.endesageracaobrasil.com.br/" /> 
		<meta name="language" content="Portuguese" /> 
		<meta name="robots" content="follow" /> 
		<meta name="googlebot" content="index, follow" >
		<title>Endesa Gera��o Brasil</title>
	</head>
	<frameset rows="99.9%,*" cols="100%" >
		<frame src="<?=$pagina;?>" id="fra_cabecalho" name="fra_cabecalho" scrolling="auto"   noresize="noresize" style="border:0;border-bottom:solid 1px #000068;">
		<frame src="rodape.php"    id="fra_rodape"    name="fra_rodape"    scrolling="no"  noresize="noresize" >
		<noframes>Esse site deve ser visualizado em um navegador que suporte "frames".</noframes>
	</frameset>