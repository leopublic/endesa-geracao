<?php

$file = urldecode($_REQUEST["arquivo"]);
if(is_null($file)) exit('invalid argument...');

//$file = str_replace("http://www.intranetgeracao.com.br","/var/www/html",$file);
//$file = str_replace("http://www.intranetendesabrasil.com.br","/var/www/html2",$file);


$arrayName = explode(".",$file);
$extension = end($arrayName);

$extension = strtolower($extension);
$ctype = "";

 switch ($extension) {
    case "pdf": $ctype="application/pdf"; break;
    case "exe": $ctype="application/octet-stream"; break;
    case "zip": $ctype="application/zip"; break;
    case "doc": $ctype="application/msword"; break;
    case "docx": $ctype="application/msword"; break;
    case "xls": $ctype="application/vnd.ms-excel"; break;
    case "xlsx": $ctype="application/vnd.ms-excel"; break;
    case "xlsm": $ctype="application/vnd.ms-excel"; break;
    case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
    case "pptx": $ctype="application/vnd.ms-powerpoint"; break;
    case "gif": $ctype="image/gif"; break;
    case "png": $ctype="image/png"; break;
    case "jpg": $ctype="image/jpg"; break;
    case "wmv": $ctype="video/x-ms-wmv"; break;
    
    default: exit("invalid argument(2)....");
 }
 
 
//exit($file); 
if(!is_readable($file)) exit("invalid argument(3)...");
 
header('Content-Description: File Transfer');
header('Content-Type: '.$ctype);
header('Content-Disposition: attachment; filename='.basename($file));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($file));
 
ob_clean();
flush();
 
readfile($file);
 
exit(0);
?>