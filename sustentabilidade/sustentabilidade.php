<?
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
    $titulo = "Sustentabilidade->Relatorios de Sustentabilidade";
        include("../header.php");
        include("../topo.inc.php");
?>

        <script language="">

            function acessar(path){

                var form = document.getElementById('frm_download');
                var hidden = document.getElementById('hd_arquivo');

                hidden.value = path;
                form.submit();
            }

        </script>


        <h1 class="tituloInternoG">Sustentabilidade</h1>
        <div style="width:790px; float:right; height:20px; background-color:#00C3F2; padding-top:5px; text-align:right"> </div>

        <div id="corpo_sustentabilidade" style="position: absolute; left: 400px; top: 170px;">
                                               
            <div id="conteudo" class="divconteudo" style='text-align:justify'>

                    <form id="frm_download" action="download.php" method="post">
                        <input type="hidden" id="hd_arquivo" name="arquivo" />
                    </form> 

                    <h1>Relatórios de Sustentabilidade 2010<u></u></h1>
                    
                    <table>
                            <tr style="border:0px">
                                <td onclick="acessar('<?=realpath("arqs/ampla_nav_2010.pdf")?>')" align="center" style="border:0px"><img src='imagens/ampla_capa_2010.jpg' width="250" /><br />Ampla</td>
                                <td onclick="acessar('<?=realpath("arqs/coelce_nav_2010.pdf")?>')" align="center" style="border:0px"><img src='imagens/coelce_capa_2010.jpg' width="250" /><br />Coelce</td>
                            </tr>
                            
                            <tr>
                                <td onclick="acessar('<?=realpath("arqs/endesa_nav_2010.pdf")?>')"  align="center" style="border:0px"><img src='imagens/brasil_capa_2010.jpg' width="250" /><br />Endesa Brasil</td>
                                <td onclick="acessar('<?=realpath("arqs/cachoeira_nav_2010.pdf")?>')"  align="center" style="border:0px"><img src='imagens/cachoeira_capa_2010.jpg' width="250" /><br />Endesa Cachoeira</td>
                            </tr>

                            <tr>
                                <td onclick="acessar('<?=realpath("arqs/cien_nav_2010.pdf")?>')" align="center" style="border:0px"><img src='imagens/cien_capa_2010.jpg' width="250" /><br />Endesa Cien</td>
                                <td onclick="acessar('<?=realpath("arqs/fortaleza_nav_2010.pdf")?>')" align="center" style="border:0px"><img src='imagens/fortaleza_capa_2010.jpg' width="250" /><br />Endesa Fortaleza</td>
                            </tr>
                    </table>

                    <h1>Relatórios de Sustentabilidade 2009</h1>
                    <table>
                            <tr style="border:0px">
                                <td onclick="acessar('<?=realpath("arqs/ampla_nav_2009.pdf")?>')" align="center" style="border:0px"><img src='imagens/ampla_capa_2009.jpg' width="250" /><br />Ampla</td>
                                <td onclick="acessar('<?=realpath("arqs/coelce_nav_2009.pdf")?>')" align="center" style="border:0px"><img src='imagens/coelce_capa_2009.jpg' width="250" /><br />Coelce</td>
                                
                            </tr>

                            <tr>
                                <td onclick="acessar('<?=realpath("arqs/endesa_nav_2009.pdf")?>')"  align="center" style="border:0px"><img src='imagens/brasil_capa_2009.jpg' width="250" /><br />Endesa Brasil</td>
                                <td onclick="acessar('<?=realpath("arqs/cachoeira_nav_2009.pdf")?>')"  align="center" style="border:0px"><img src='imagens/cachoeira_capa_2009.jpg' width="250" /><br />Endesa Cachoeira</td>
                            </tr>

                            <tr>
                            
                                <td onclick="acessar('<?=realpath("arqs/cien_nav_2009.pdf")?>')" align="center" style="border:0px"><img src='imagens/cien_capa_2009.jpg' width="250" /><br />Endesa Cien</td>
                                <td onclick="acessar('<?=realpath("arqs/fortaleza_nav_2009.pdf")?>')" align="center" style="border:0px"><img src='imagens/fortaleza_capa_2009.jpg' width="250" /><br />Endesa Fortaleza</td>
                            </tr>
                    </table>

                    <h1>Relatórios de Sustentabilidade 2008</h1>
                    <table>
                            <tr style="border:0px">
                                <td onclick="window.location.href='arqs/ampla_nav_2008/index.html';" align="center" style="border:0px"><img src='imagens/ampla_capa_2008.jpg' width="250" /><br />Ampla</td>
                                <td onclick="window.location.href='arqs/coelce_nav_2008/index.html';" align="center" style="border:0px"><img src='imagens/coelce_capa_2008.jpg' width="250" /><br />Coelce</td>
                                
                            </tr>

                            <tr>
                                <td onclick="window.location.href='arqs/endesa_nav_2008/index.html';" align="center" style="border:0px"><img src='imagens/brasil_capa_2008.jpg' width="250" /><br />Endesa Brasil</td>
                                <td onclick="window.location.href='arqs/cachoeira_nav_2008/index.html';" align="center" style="border:0px"><img src='imagens/cachoeira_capa_2008.jpg' width="250" /><br />Endesa Cachoeira</td>
                            </tr>

                            <tr>
                                <td onclick="window.location.href='arqs/cien_nav_2008/index.html';" align="center" style="border:0px"><img src='imagens/cien_capa_2008.jpg' width="250" /><br />Endesa Cien</td>
                                <td onclick="window.location.href='arqs/fortaleza_nav_2008/index.html';" align="center" style="border:0px"><img src='imagens/fortaleza_capa_2008.jpg' width="250" /><br />Endesa Fortaleza</td>
                            </tr>
                    </table>

                    <!--
                    <h1>Relatórios de Sustentabilidade 2007</h1>
                    <table>
                            <tr style="border:0px">
                                    <td align="center" style="border:0px"><a href="http://www.endesabrasil.com.br/sust/ampla/index.html"><img src="imagens/sustentabilidade-ampla.jpg" border="0" width="250px" /></a><br />Ampla</td>
                                    <td align="center" style="border:0px"><a href="http://www.endesabrasil.com.br/sust/coelce/index.html"><img src="imagens/sustentabilidade-coelce.jpg" border="0" width="250px" /></a><br />Coelce</td>
                            </tr>
                            <tr>
                                    <td align="center" style="border:0px"><a href="http://www.endesabrasil.com.br/sust/endesa/index.html"><img src="imagens/sustentabilidade-endesabras.jpg" border="0" width="250px" /></a><br />Endesa Brasil</td>
                                    <td align="center" style="border:0px"><a href="http://www.endesabrasil.com.br/sust/cachoeira/index.html"><img src="imagens/sustentabilidade-cachoeira.jpg" border="0" width="250px" /></a><br />Endesa Cachoeira</td>
                            </tr>
                            <tr>
                                    <td align="center" style="border:0px"><a href="http://www.endesabrasil.com.br/sust/cien/index.html"><img src="imagens/sustentabilidade-cien.jpg" border="0" width="250px" /></a><br />Endesa Cien</td>
                                    <td align="center" style="border:0px"><a href="http://www.endesabrasil.com.br/sust/fortaleza/index.html"><img src="imagens/sustentabilidade-fortaleza.jpg" border="0" width="250px" /></a><br />Endesa Fortaleza</td>
                            </tr>
                    </table>-->
            </div>
    </div>
        
   </div>

</body>
</html>