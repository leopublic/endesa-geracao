<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt">
	<head>
		<title><?php print $titulo; ?></title>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-20098935-6']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<style type="text/css" media="screen">
			*{
				font-family: Arial, Helvetica;
				font-size: 12px;
				color:#325275;
			}
			.menu{
				width:100%;
				margin:0px;
				padding:0px;
				list-style-type: none;
			}
			
			.menu li{
				text-align: left;
				color:#0C286C;
				font-weight: bold;
				margin:2px 0px 2px;
				padding:5px 0px 5px;
				cursor:pointer;
			}
            
            .menu li a{
                color:#0C286C;
                text-decoration: none;
            }
			
			.menu li:hover{
				color:#00C3F2;				
			}
			
			
			.menu li.box{
				border:1px solid #00C3F2;
				color:#0C286C;
				font-weight: bold;
				margin:3px 0px 3px;
				padding:5px 0px 5px;
				cursor:pointer;
				text-align:center;
			}

			.menu li.boxAberto{
				border:1px solid #00C3F2;
				background-color: #00C3F2;
				color:#0C286C;
				font-weight: bold;
				margin:3px 0px 3px;
				padding:5px 0px 5px;
				cursor:pointer;
				text-align:center;
			}
	
			.menu li.box:hover{
				background-color: #00C3F2;
				color:#0C286C;
			}

			.menu li.interno{
				padding-left: 15px;
				background:url(imagens/bullet.jpg) 0px 7px no-repeat white;
				color: #00C3F2;
			}
			
			.menu li.interno a{
				color: #00C3F2;
			}
			
			.lista{
				margin-left:10px;
				padding:0px;
				list-style-type: none;
			}
			
			.lista li{
				padding: 3px;
				padding-left: 15px;
				background:url(imagens/bullet.jpg) 0px 5px no-repeat white;
			}

			.menu li.separador hr{
				border:2px solid #00C3F2;
			}
			
			.tituloInterno{
				width:100%;
				text-align:left;
				color: #00C3F2;
				font-size: 18px;
				font-weight: bold;
			}
			
			.tituloInternoG{
				width:100%;
				text-align:right;
				color: #00C3F2;
				font-size: 22px;
				font-weight: bold;
				margin:0px;
			}
			
			.destaque{
				width:100%;
				text-align:center;
				color: #00C3F2;
				font-weight: bold;
			}
			
			table.login{
				border:1px solid #00C3F2;
			}
			
			table.login td.tdTitu{
				width:40px;
				text-align:right;
			}
			
			table.login td.tdCampos{padding-top:5px; padding-left:15px; text-align:left;}
			
			table.login td.tdBt{padding:5px; text-align:left;}
			
			table.login td.tdCampos input.cpLogin{width:120px; border:1px solid #999;}
			
			table.login td.tdBt input.btLogin{background:#E6E6E6; color:#666;}
			 
			a.lkEsquece { 
				text-decoration:none; 
			}


                        #corpo_sustentabilidade h1 {
                                    font-size:16pt;
                                    font-weight	:bold;
                                    color:#5288B4;
                                    text-align:center;
                                    padding:0 4px 4px 4px;
                                    margin-bottom:15px;
                                    border-bottom: 2px solid #5288B4;
                                }

                        #corpo_sustentabilidade    td {
                                color: #666666;
                                font-size: 12pt;
                                font-weight: normal;
                                text-decoration: none;
                            }

                        #corpo_sustentabilidade    td:hover {

                                cursor: pointer;
                                text-decoration: underline;
                            }

		</style>
		<script type="text/javascript" language="JavaScript" src="js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" language="JavaScript" src="js/jquery-ui-1.8.22.custom.min.js" ></script>
		<script type="text/javascript" language="JavaScript">
			function fechaTodos() {
			    var elements = document.getElementsByTagName("div");
			    var li_elements = document.getElementsByTagName("li");
			    var result = [];
			
			    for (var i = 0, len = elements.length; i < len; i++) {
			        if (elements[i].className == 'subMenu') {
			            elements[i].style.display = 'none';
			        }
                }
			    
                for (var i = 0, len = li_elements.length; i < len; i++) {
			        if (li_elements[i].className == 'boxAberto') {
			            li_elements[i].className = 'box';
			        }			        
			    }
			
			    return result;
			}
					
			function abreMenu(id1,dad){
				var obj = document.getElementById('div_'+id1);
				if(obj.style.display=='none'){
					fechaTodos();
					dad.className = 'boxAberto';
					obj.style.display = 'block';
				}else{
					dad.className = 'box';
					obj.style.display = 'none';
				}
			}
			
			function redirecionar(pagina){ 
				if(pagina!=''){ 
					window.location.href=pagina; 
				}
			}
			var options = {};
			$(document).ready(function (){
				// $("#p8").css("background-color", "#00C3F2");
				// $("#p8").effect("pulsate", { times:20 }, 1000);
				// $("#p4").css("background-color", "#00C3F2");
				// $("#p4").effect("pulsate", { times:20 }, 1000);

			});
//		$( "#m6" ).effect( "pulsate", options, 500 );
		</script>
	</head>
	<body style="text-align:center;">
		<div style="width:960px; text-align:justify; margin:20px auto;" >
			<div style="width:100%; float:left;">
				<a href="inicio.php" target="_self" style="border:none;"><img src="imagens/geracao_br.jpg"  style="border:none;"/></a>
				<br />&nbsp;
			</div>
			<div style="width:160px; float:left;">
				<ul class="menu">
                    <?
                        $res = $bd->executa("SELECT cateOrdem, cateDescricao, cate_fl_link_direto, categoria.menuId menuId_link_direto, menu.cateId, menu.menuId, menuOrdem, menuDescricao FROM menu INNER JOIN categoria ON categoria.cateId = menu.cateId  WHERE emprId = 1 and cate_visivel = 1 ORDER BY cateOrdem, menuOrdem");
                        $div = 0;
                        while($row=mysql_fetch_object($res)){
                            if($categoria!=$row->cateDescricao){
                                $categoria = $row->cateDescricao;
                                if($categoria!='Menu Raiz - Sem grupo'){
                                    if($div!=0){
                                        print '</div>';
                                    }
									if ($row->cate_fl_link_direto){
	                                    print '<li id="p'.$row->cateId.'" class="box" onclick="javascript:window.location=\'pagina.php?menuId='.$row->menuId_link_direto.'\';">'.$categoria.'</li>'."\r";
									}
									else{
	                                    print '<li id="p'.$row->cateId.'" class="box" onclick="abreMenu('.$row->cateId.',this);">'.$categoria.'</li>'."\r";
									}
                                    print '<div id="div_'.$row->cateId.'" class="subMenu" style="display:none">'."\r";
                                    $div++;
                                    $class = 'class="interno"';
                                }else{
                                    if($div!=0){
                                        print '</div>';
                                    }
									print '<li class="separador"><hr /></li>';
                                    $class = '';
                                }
								if (!$row->cate_fl_link_direto){
	                                print '<li id="m'.$row->menuId.'" '.$class.'><a href="pagina.php?menuId='.$row->menuId.'">'.$row->menuDescricao.'</a></li>'."\r";
								}
                            } else{
                                print '<li id="m'.$row->menuId.'" '.$class.'><a href="pagina.php?menuId='.$row->menuId.'">'.$row->menuDescricao.'</a></li>'."\r"; 
                            }
                        }
                    ?>
					<li class="separador"><hr /></li>
					<li onclick="redirecionar('inicio.php');">P&aacute;gina Principal</li>
    				</ul>
			</div>